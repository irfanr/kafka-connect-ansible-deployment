Deployment automation Ansible script for Allianz Dukcapil Project.

# How to use

>ansible-playbook deploy.yml -i inventory/hosts -e host=dev -e role=azid.irfanromadona.executeshell -u it-ops

ansible-playbook playbooks/restart-service.yml \
-i inventory/hosts \
-e host=dev \
-u automation \
-e env=dev

ansible-playbook playbooks/microservices/deploy-all.yml \
-i inventory/hosts \
-e host=uat \
-u it-ops \
-e service_name=nik-report-service \
-e git_tag=0.1.1-SNAPSHOT \
-e 'nexus_username="Irfan Romadona"' \
-e nexus_password=****** \
-e 'git_username="Irfan Romadona"' \
-e git_password==****** \
-e env=uat

ansible-playbook playbooks/microservices/deploy-download.yml \
-i inventory/hosts \
-e host=uat \
-u it-ops \
-e service_name=nik-ktp-service \
-e git_tag=0.1.0-RELEASE \
-e 'nexus_username="Irfan Romadona"' \
-e nexus_password==****** \
-e 'git_username="Irfan Romadona"' \
-e git_password==****** \
-e env=uat

ansible-playbook playbooks/microservices/deploy-single.yml \
-i inventory/hosts \
-e host=uat \
-u it-ops \
-e service_name=nik-eureka-server \
-e git_tag=0.0.18-SNAPSHOT \
-e 'nexus_username="Irfan Romadona"' \
-e nexus_password=azlife193* \
-e 'git_username="Irfan Romadona"' \
-e git_password=****** \
-e env=uat \
-e role=azid.nik.update_service

ansible-playbook playbooks/microservices/git_tag_recreate.yml \
-i inventory/hosts \
-e host=uat \
-u it-ops \
-e service_name=nik-batch-service \
-e git_tag=HEAD \
-e git_tag_new=0.1.4-SNAPSHOT \
-e 'nexus_username="Irfan Romadona"' \
-e nexus_password=azlife194* \
-e 'git_username="Irfan Romadona"' \
-e git_password=azlife194* \
-e env=dev

ansible-playbook playbooks/microservices/maven_update_version.yml \
-i inventory/hosts \
-e host=uat \
-u it-ops \
-e service_name=nik-eureka-server \
-e git_tag=HEAD \
-e git_tag_new=0.1.1-SNAPSHOT \
-e 'nexus_username="Irfan Romadona"' \
-e nexus_password==****** \
-e 'git_username="Irfan Romadona"' \
-e git_password==****** \
-e env=uat

*Note
- Versioning in POM should same like Git Tag Version
